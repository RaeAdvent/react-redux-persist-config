import React from 'react'

import { ExampleComponent } from 'react-redux-persist-config'
import 'react-redux-persist-config/dist/index.css'

const App = () => {
  return <ExampleComponent text="Create React Library Example 😄" />
}

export default App
