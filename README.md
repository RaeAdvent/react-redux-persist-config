# react-redux-persist-config

> Simple Plugins for seamlessly configure react with redux and redux-persist

[![NPM](https://img.shields.io/npm/v/react-redux-persist-config.svg)](https://www.npmjs.com/package/react-redux-persist-config) [![JavaScript Style Guide](https://img.shields.io/badge/code_style-standard-brightgreen.svg)](https://standardjs.com)

## Install

```bash
npm install --save react-redux-persist-config
yarn add react-redux-persist-config
```

## Usage (createConfig & ReduxPersist) :

```jsx
import { createConfig, ReduxPersist } from 'react-redux-persist-config'

// import reducers
import reducerA from 'store/reducers/reducerA'
import reducerB from 'store/reducers/reducerB'

const reducers = {
  a: reducerA,
  b: reducerB,
}

//create persist store config
const [store, persistor] = createConfig('nameOfPersistState', reducers)

// Loading animation (Optional)
const Loading = () => <div>Loading</div>

const App = () => {
  return (
    <ReduxPersist store={store} persistor={persistor} loading={<Loading />}>
      <App />
    </ReduxPersist>
  )
}
```

## Usage reducerLoader :

in your reducer .js file :

```js
import { reducerLoader } from 'react-redux-persist-config'
import  {ACTION_A, ACTION_B, ACTION_C } from 'store/actions/action.js'

const initialState = {
  stateA: null,
  stateB: filled
}

// be advised the reducers is using object as reducer function, otherwise reducerLoader will cause error
// example reducer

const reducerCollection = {}

reducerCollection.ACTION_A  = (state, action) => {
  //...your usual state management logic
}

reducerCollection.ACTION_B  = (state, action) => {
  ...
}

// then pass initial state and reducer logic to reducerLoader
const reducerA = (initial, collections) => {
  return reducerLoader(initial, collections)
}

// export reducer
export default reducerA(initialState, reducerCollection)

```

## License

MIT © [Raeneldis A. Sadra]
