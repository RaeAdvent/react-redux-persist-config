import { combineReducers, createStore } from 'redux'
import { persistStore, persistReducer } from 'redux-persist'

import storageSession from 'redux-persist/lib/storage/session'
import storage from 'redux-persist/lib/storage'

import hardSet from 'redux-persist/lib/stateReconciler/hardSet'
import autoMergeLevel1 from 'redux-persist/lib/stateReconciler/autoMergeLevel1'
import autoMergeLevel2 from 'redux-persist/lib/stateReconciler/autoMergeLevel2'

const reconcilers = {
  hardSet: hardSet,
  autoMergeLevel2: autoMergeLevel2,
}

const createConfig = (
  keyName,
  combine,
  storageType = 'default',
  stateRecon = 'default'
) => {
  const persistConfig = {
    key: keyName,
    storage: storageType === 'default' ? storage : storageSession,
    stateReconciler:
      stateRecon === 'default' ? autoMergeLevel1 : reconcilers[stateRecon],
  }

  const rootReducer = combineReducers({
    ...combine,
  })

  const reducersPersist = persistReducer(persistConfig, rootReducer)

  const store = createStore(reducersPersist)
  const persistor = persistStore(store)

  return [store, persistor]
}

export default createConfig
