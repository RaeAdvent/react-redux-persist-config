const reducerLoader = (initial, collections) => {
  return (state = initial, action) => {
    const ReducerDictionary = collections[action.type]

    if (!ReducerDictionary || !action || !action.type) {
      return state
    }

    return ReducerDictionary(state, action)
  }
}

export default reducerLoader
