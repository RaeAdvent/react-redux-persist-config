import React from 'react'
import reducerLoader from './lib/reducerLoader'

import createConfig from './lib/StoreConfig'

import { Provider } from 'react-redux'
import { PersistGate } from 'redux-persist/lib/integration/react'

const ReduxPersist = ({ children, store, persistor, loading }) => {
  return (
    <Provider store={store}>
      <PersistGate loading={loading ? loading : null} persistor={persistor}>
        {children}
      </PersistGate>
    </Provider>
  )
}

export { reducerLoader, createConfig, ReduxPersist }
